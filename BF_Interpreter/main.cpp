/*	Interpreter for the programming language 'BrainFuck'
	Made in October 2018, by Daniel Monastirski */
#include <stdio.h>
#include <iostream>
#include <string>
#include <stack>
#include <fstream>
#include <sstream>

int main(int argc, char* argv[])
{
	if (argc != 2)
	{
		std::cout << "Usage: " << argv[0] << " [file]" << std::endl;
		system("PAUSE");
		return 0;
	}
	std::ifstream f(argv[1]);
	if (!f.is_open())
		throw std::runtime_error("Cannot open file");
	std::stringstream buffer;
	buffer << f.rdbuf();
	std::string input = buffer.str();
	char data[30000] = { 0 };
	char* dp = data;
	std::string::const_iterator ip = input.begin();
	std::stack<std::string::const_iterator> ip_stack;
	int debug = 0;
	while (ip != input.end())
	{
		switch (*ip)
		{
		case '>':
			dp++;
			break;
		case '<':
			dp--;
			break;
		case '+':
			(*dp)++;
			break;
		case '-':
			(*dp)--;
			break;
		case '.':
			putchar(*dp);
			break;
		case ',':
			std::cout << "Awaiting input:";
			*dp = getchar();
			break;
		case '[':
			if ((*dp) != 0)
				ip_stack.push(ip);
			break;
		case ']':
			if ((*dp) != 0)
				ip = ip_stack.top();
			else
				ip_stack.pop();
			break;
		default:
			break;
		}
		ip++;
	}
	system("PAUSE");
	return 0;
}
