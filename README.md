A simple interpreter for the coding language *BrainFuck* (See: https://en.wikipedia.org/wiki/Brainfuck).
To use the program, you must give the program a file containing the relevant code.